#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/irqreturn.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/clk.h>
#include <linux/device.h>
#include <linux/semaphore.h>
#include <linux/kdev_t.h>
#include <asm/io.h>

#include "SPI.h"

#include "spi_user_interface.h"
#include "spi_kernel_module.h"

MODULE_LICENSE("GPL");

typedef struct {
    bool busy;
    struct {
        volatile unsigned int curr_index;
        volatile char tx_buffer[SPI_MAX_BUFFER_SIZE];
        volatile char rx_buffer[SPI_MAX_BUFFER_SIZE];
        volatile unsigned int transfer_len;
    } buffer;
    unsigned int cpu_hz;
    unsigned int *ptr_SPI_REG_BASE;
    unsigned int *ptr_GPIO_REG_BASE;
    struct semaphore transfer_finished_lock;
    unsigned int dev_ID;
} spi_context_t;

static spi_context_t g_spi_ctxt;

static irqreturn_t spi_hw_interrupt(int irq, void *dev_id)
{
    char tx_char = 0, rx_char = 0;
    if (g_spi_ctxt.buffer.curr_index < g_spi_ctxt.buffer.transfer_len) {
        tx_char = g_spi_ctxt.buffer.tx_buffer[g_spi_ctxt.buffer.curr_index];
        SPI_FIFO_DATA_ONLY_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, tx_char);
        printk(KERN_INFO "spi_hw_interrupt: Write curr-idx [%d], val = 0x%x \n", g_spi_ctxt.buffer.curr_index, tx_char);
        if (g_spi_ctxt.buffer.curr_index != 0) {
            rx_char = SPI_FIFO_DATA_READ(g_spi_ctxt.ptr_SPI_REG_BASE);
            g_spi_ctxt.buffer.rx_buffer[g_spi_ctxt.buffer.curr_index-1] = rx_char;
            printk(KERN_INFO "spi_hw_interrupt: Read curr-idx [%d], val = 0x%x \n", g_spi_ctxt.buffer.curr_index-1, rx_char);
        }
        g_spi_ctxt.buffer.curr_index += 1;
    } else {
        /* All data transferred, now release the semaphore */
        rx_char = SPI_FIFO_DATA_READ(g_spi_ctxt.ptr_SPI_REG_BASE);
        g_spi_ctxt.buffer.rx_buffer[g_spi_ctxt.buffer.curr_index-1] = rx_char;
        printk(KERN_INFO "spi_hw_interrupt: Read curr-idx [%d], val = 0x%x \n", g_spi_ctxt.buffer.curr_index-1, rx_char);
        printk(KERN_INFO "spi_hw_interrupt: Finished [%d]\n", g_spi_ctxt.buffer.curr_index);
        up(&g_spi_ctxt.transfer_finished_lock);
        SPI_CS_TA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
      mb();
    }
    return IRQ_HANDLED;
}

int spi_device_open_fn (struct inode *node, struct file *f_ptr) {
    if (g_spi_ctxt.busy) {
        /* SPI driver busy since someone is using it, dont allow further use */
        printk(KERN_INFO "spi_device_open_fn: Failed\n");
        return -EBUSY;
    } else {
        printk(KERN_INFO "spi_device_open_fn: Succeeded\n");
        g_spi_ctxt.busy = true;
        /* TODO: Complete SPI hardware initialization and reset its speed and cs pins */
        return 0; /* success */
    }
}
int spi_device_release_fn (struct inode *node, struct file *f_ptr) {
    if (g_spi_ctxt.busy) {
        printk(KERN_INFO "spi_device_release_fn: Succeeded\n");
        /* Stop the SPI transactions if in progress */
        SPI_CS_TA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
        mb();

        /* Release the SPI hardware and unset the busy flag and return */
        g_spi_ctxt.busy = false;
        
        return 0;
    } else {
        printk(KERN_INFO "spi_device_release_fn: Failed\n");
        return -EPERM;
    }
}

ssize_t spi_device_read_fn (struct file *f_ptr, char *buf, size_t size, loff_t *offset) {
    /* We dont support reading directly from this device since its based on transactions */
    printk(KERN_INFO "spi_device_read_fn: Called\n");
    return 0;
}

ssize_t spi_device_write_fn (struct file *f_ptr, const char *buf, size_t size, loff_t *offset) {
    /* We dont support writing directly to this device since its based on transactions */
    printk(KERN_INFO "spi_device_write_fn: Called\n");
    return 0;
}

void gpio_mode_setup(unsigned int gpio, unsigned int mode) {
   int reg, shift;
   reg   =  gpio/10;
   shift = (gpio%10) * 3;
   g_spi_ctxt.ptr_GPIO_REG_BASE[reg] = (g_spi_ctxt.ptr_GPIO_REG_BASE[reg] & ~(7<<shift)) | (mode<<shift);
}

long spi_device_ioctl_fn(struct file *f_ptr, unsigned int cmd, unsigned long arg) {
    long ret = 0;
    long i;
    unsigned int cdiv = 0;
    printk(KERN_INFO "spi_device_ioctl_fn: Called\n");
    if (g_spi_ctxt.busy) {
        spi_transaction_t spi_transaction = {0};
        ret = copy_from_user((void*)&spi_transaction, (void*)arg, sizeof(spi_transaction_t));
        /* Print transfer information on to the terminal */
        printk(KERN_INFO "spi_transaction->\n");
        printk(KERN_INFO "  speed       = %d\n", spi_transaction.speed);
        printk(KERN_INFO "  mode        = %d\n", spi_transaction.mode);
        printk(KERN_INFO "  cs          = %d\n", spi_transaction.cs);
        printk(KERN_INFO "  transfer-len= %d\n", spi_transaction.transfer_len);

        if (spi_transaction.write_data) {
            ret = copy_from_user((void*)g_spi_ctxt.buffer.tx_buffer, (void*)spi_transaction.write_data, spi_transaction.transfer_len);
            printk(KERN_INFO "spi_device_ioctl_fn: Writing data-sequence \n");
            for (i=0; i<spi_transaction.transfer_len; i++) {
                printk("0x%x, ", g_spi_ctxt.buffer.tx_buffer[i]);
            }
            printk(KERN_INFO "\n");
        }

        if (spi_transaction.transfer_len==0) {
            ret = -1;
            return ret;
        }
        
        g_spi_ctxt.buffer.transfer_len = spi_transaction.transfer_len;
        g_spi_ctxt.buffer.curr_index = 0;

        /* Configure SPI_CS register */
        switch (spi_transaction.cs) {
            case SPI_CS_SEL_0: SPI_CS_CS_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0); break;
            case SPI_CS_SEL_1: SPI_CS_CS_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1); break;
            default:
                printk(KERN_INFO "Warning: Invalid CS selected, using lowest value\n");
                SPI_CS_CS_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0); 
                break;
        }

        switch (spi_transaction.mode) {
            case SPI_MODE_0:
                SPI_CS_CPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                SPI_CS_CPHA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                break;
            case SPI_MODE_1:
                SPI_CS_CPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);
                SPI_CS_CPHA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                break;
            case SPI_MODE_2:
                SPI_CS_CPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                SPI_CS_CPHA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);
                break;
            case SPI_MODE_3:
                SPI_CS_CPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);
                SPI_CS_CPHA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);
                break;
            default:
                SPI_CS_CPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                SPI_CS_CPHA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
                break;
        }

        SPI_CS_CLEAR_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0x3);

        SPI_CS_CSPOL_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_TA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_DMAEN_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_INTD_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);

        SPI_CS_INTR_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_REN_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_LEN_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        SPI_CS_CSPOL0_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
        SPI_CS_CSPOL1_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);
        SPI_CS_CSPOL2_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 0);

        switch (spi_transaction.speed) {
            case SPI_SPEED_500Hz: 
                cdiv = g_spi_ctxt.cpu_hz/500;
                break;
            case SPI_SPEED_1KHz: 
                cdiv = g_spi_ctxt.cpu_hz/1000;
                break;
            case SPI_SPEED_2KHz: 
                cdiv = g_spi_ctxt.cpu_hz/2000;
                break;
            default:
                cdiv = g_spi_ctxt.cpu_hz/500;
                break;
        }

        SPI_CLK_CDIV_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, cdiv);

        /* Lock the semaphore and wait for interrupt to unlock and finish it */
        if (down_interruptible(&g_spi_ctxt.transfer_finished_lock)) {
            printk(KERN_INFO "spi_device_ioctl_fn: transfer_finished_lock lock-failed\n");
            return -EINTR;
        } else {
            printk(KERN_INFO "spi_device_ioctl_fn: transfer_finished_lock locked\n");
            /* Lock succeeded as expected */
        }

        /* Generate result according to the requested IOCTL call */
        switch (cmd) {
            case SPI_IO_TRANSACTION:
                mb();
                SPI_CS_TA_WRITE(g_spi_ctxt.ptr_SPI_REG_BASE, 1);
                mb();
                break;
            default:
                ret = -1;
                break;
        }

        printk(KERN_INFO "spi_device_ioctl_fn: transfer_finished_lock waiting\n");
        if (down_interruptible(&g_spi_ctxt.transfer_finished_lock)) {
            return -EINTR;
        } else {
            /* Lock succeeded as expected */
            /* Unlock the semaphore as its job is done, and can be used next time */
            printk(KERN_INFO "spi_device_ioctl_fn: transfer_finished_lock unlocked\n");
            up(&g_spi_ctxt.transfer_finished_lock);

            /* Copy the received data into the receive buffer if valid */
            if (spi_transaction.read_data) {
                printk(KERN_INFO "spi_device_ioctl_fn: Read data-sequence \n");
                for (i=0; i<spi_transaction.transfer_len; i++) {
                    printk("0x%x, ", g_spi_ctxt.buffer.rx_buffer[i]);
                }
                printk(KERN_INFO "\n");
                ret = copy_to_user((void*)spi_transaction.read_data, (void*)g_spi_ctxt.buffer.rx_buffer,  spi_transaction.transfer_len);
            }

            /* Print some CS register contents */
            printk(KERN_INFO "spi_device_ioctl_fn: RXF = %d, RXR = %d, TXD = %d, RXD = %d, DONE = %d",
                    SPI_CS_RXF_READ(g_spi_ctxt.ptr_SPI_REG_BASE), 
                    SPI_CS_RXR_READ(g_spi_ctxt.ptr_SPI_REG_BASE), 
                    SPI_CS_TXD_READ(g_spi_ctxt.ptr_SPI_REG_BASE), 
                    SPI_CS_RXD_READ(g_spi_ctxt.ptr_SPI_REG_BASE),
                    SPI_CS_DONE_READ(g_spi_ctxt.ptr_SPI_REG_BASE));
        }
    } else {
        ret = -1;
    }
    return ret;
}


struct file_operations spi_kernel_fops = {
    .read = spi_device_read_fn,
    .write = spi_device_write_fn,
    .unlocked_ioctl = spi_device_ioctl_fn,
    .open = spi_device_open_fn,
    .release = spi_device_release_fn,
};

static dev_t first;
static int major = -1;
static struct cdev c_dev;
static struct class *cl = NULL;
static struct device *dev = NULL;

static int __init spi_kernel_module_init(void)
{
    int res;
    if(alloc_chrdev_region(&first, 0, 1, DEVICE_FILE_NAME)) {
        printk( KERN_ALERT "spi_kernel_module_init: Device Registration failed\n" );
        return -1;
    } else {
        major = MAJOR(first);
        printk( KERN_ALERT "spi_kernel_module_init: Device created with Major number is: %d\n", res);
    }
    if ((cl = class_create(THIS_MODULE, DEVICE_FILE_NAME))==NULL) {
        unregister_chrdev_region(first, 1);
        return -1;
    }
    if ((dev = device_create(cl, NULL, first, NULL, DEVICE_FILE_NAME))==NULL) {
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }
    cdev_init(&c_dev, &spi_kernel_fops);
 
    if( cdev_add(&c_dev, first, 1) == -1)
    {
        printk( KERN_ALERT "spi_kernel_module_init: Device addition failed\n" );
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }

    if (request_irq(SPI_IRQ_NUMBER, spi_hw_interrupt, SPI_IRQ_FLAGS, DEVICE_FILE_NAME, &g_spi_ctxt.dev_ID)) {
        printk(KERN_ERR "spi_kernel_module_init: Cannot register IRQ %d\n", SPI_IRQ_NUMBER);
        cdev_del(&c_dev);
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -EIO;
    } else {
        printk(KERN_INFO "spi_kernel_module_init: registered IRQ %d\n", SPI_IRQ_NUMBER);
    }

	printk(KERN_INFO "spi_kernel_module_init: SPI-Initialized\n");

    /* Initialize the driver context */
    g_spi_ctxt.busy = false;
    g_spi_ctxt.cpu_hz = SPI_SYSTEM_CORE_CLOCK_HZ;
	printk(KERN_INFO "spi_kernel_module_init: Core Clock = %d\n", g_spi_ctxt.cpu_hz);

    /* Initialized semaphore of the context */
    sema_init(&g_spi_ctxt.transfer_finished_lock,  1 /* count =1 */);

    g_spi_ctxt.ptr_SPI_REG_BASE = (unsigned int *)ioremap(SPI_REG_BASE, SPI_REG_LEN);
    g_spi_ctxt.ptr_GPIO_REG_BASE = (unsigned int *)ioremap(GPIO_REG_BASE, GPIO_REG_LEN);

    /* Initialize GPIO mode registers for SPI operation */
    gpio_mode_setup(SPI_GPIO_PIN_CE0, SPI_GPIO_MODE);
    gpio_mode_setup(SPI_GPIO_PIN_CE1, SPI_GPIO_MODE);
    gpio_mode_setup(SPI_GPIO_PIN_SCLK, SPI_GPIO_MODE);
    gpio_mode_setup(SPI_GPIO_PIN_MISO, SPI_GPIO_MODE);
    gpio_mode_setup(SPI_GPIO_PIN_MOSI, SPI_GPIO_MODE);
	return 0;
}

static void __exit spi_kernel_module_exit(void)
{
    free_irq(SPI_IRQ_NUMBER, &g_spi_ctxt.dev_ID);
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    
    printk(KERN_INFO "spi_kernel_module_exit: Freed IRQ %d\n", SPI_IRQ_NUMBER);
	printk(KERN_INFO "spi_kernel_module_exit: SPI-Deinitialized\n");

}

module_init(spi_kernel_module_init);
module_exit(spi_kernel_module_exit);
