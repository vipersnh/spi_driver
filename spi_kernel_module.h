#ifndef __SPI_KERNEL_MODULE_H__
#define __SPI_KERNEL_MODULE_H__

#define SPI_MAX_BUFFER_SIZE 1024

/* Document : BCM2835-ARM-Peripherals.pdf */

#define SPI_IRQ_NUMBER 84
#define SPI_IRQ_FLAGS  0

/* Hardware Register Addresses */

#define SPI_REG_BASE   (long long unsigned int)0x3F204000
#define SPI_REG_LEN    (long long unsigned int)1024

#define GPIO_REG_BASE   (long long unsigned int)0x3F200000
#define GPIO_REG_LEN    (long long unsigned int)1024

#define SPI_SYSTEM_CORE_CLOCK_HZ (1200000000/2)

/* SPI GPIO PINS. */

#define SPI_GPIO_PIN_CE0   8
#define SPI_GPIO_PIN_CE1   7
#define SPI_GPIO_PIN_SCLK 11
#define SPI_GPIO_PIN_MISO  9
#define SPI_GPIO_PIN_MOSI 10

/* SPI GPIO MODE */
#define SPI_GPIO_MODE 4 /* Alternate 0 */

typedef enum {
    SPI_IO_TRANSACTION = 1,
} spi_ioctl_enum_t;

typedef struct {
    spi_interface_speed_enum_t speed;
    spi_interface_mode_enum_t mode;
    spi_interface_cs_enum_t cs;
    char *write_data;
    char *read_data;
    unsigned int transfer_len;
} spi_transaction_t;

#endif
