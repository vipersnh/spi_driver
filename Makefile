KDIR := /home/vipersnh/linux

obj-m += spi_kernel_module.o
all: 
	make -C ${KDIR}/build M=$(PWD) modules 
clean: 
	make -C ${KDIR}/build M=$(PWD) clean 

user_program:
	echo "User Program compilation"
	arm-linux-gnueabihf-gcc -Wall -I./ user_program.c spi_user_interface.c -o user_program.elf
