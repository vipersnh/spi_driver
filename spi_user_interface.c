#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "spi_user_interface.h"
#include "spi_kernel_module.h"

static int g_spi_dev_file;
static spi_transaction_t g_spi_transaction;

#define SPI_DEV_NAME "/dev/" DEVICE_FILE_NAME

bool spi_interface_init(spi_interface_speed_enum_t speed, spi_interface_mode_enum_t mode)
{
    if (g_spi_dev_file == 0) {
        g_spi_dev_file = open(SPI_DEV_NAME, 0);
        if (g_spi_dev_file < 0) {
            printf ("Can't open device file: %s\n", SPI_DEV_NAME);
            exit(-1);
        } else {
            printf("Opened device file: %s\n", SPI_DEV_NAME);
        }
        /* Initialize the SPI speed and mode */
        g_spi_transaction.speed = speed;
        g_spi_transaction.mode = mode;
        return true;
    } else {
        printf("File already open: %s\n", SPI_DEV_NAME);
        return false;
    }
}

bool spi_interface_deinit()
{
    if (g_spi_dev_file) {
        close(g_spi_dev_file);
        printf("Closed device file: %s\n", DEVICE_FILE_NAME);
        g_spi_dev_file = 0;
        return true;
    } else {
        printf("File already close: %s\n", DEVICE_FILE_NAME);
        return false;
    }
}

bool spi_write_data(char *write_data, unsigned int write_len, 
        spi_interface_cs_enum_t cs)
{
    return spi_read_write_data(NULL, write_data, write_len, cs);
}


bool spi_read_data(
        char *read_data, 
        unsigned int read_len, 
        spi_interface_cs_enum_t cs
        )
{
    return spi_read_write_data(read_data, NULL, read_len, cs);
}

bool spi_read_write_data(
        char *read_data, 
        char *write_data, 
        unsigned int transfer_len, 
        spi_interface_cs_enum_t cs
        )
{
    int ret;
    if (g_spi_dev_file) {
        g_spi_transaction.cs = cs;
        g_spi_transaction.write_data = write_data;
        g_spi_transaction.read_data = read_data;
        g_spi_transaction.transfer_len = transfer_len;
        ret = ioctl(g_spi_dev_file, SPI_IO_TRANSACTION, &g_spi_transaction);
        if (ret==0) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }    
}

