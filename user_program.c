#include "stdio.h"
#include "stdbool.h"
#include "spi_user_interface.h"

#define SPI_BUFFER_LEN 20

int main() {
    int index;
    char write_data[SPI_BUFFER_LEN] = "12345678";
    char read_data[SPI_BUFFER_LEN] = "12345678";
    spi_interface_cs_enum_t cs = SPI_CS_SEL_1;
    spi_interface_init(SPI_SPEED_2KHz, SPI_MODE_1);
    spi_read_write_data(read_data, write_data, SPI_BUFFER_LEN, cs);
    printf("Operating a read-write SPI transaction\n");
    for (index=0; index < SPI_BUFFER_LEN; index++) {
        printf("[%02d]    write = 0x%02x, read = 0x%02x\n", index, write_data[index], read_data[index]);
    }
    printf("Finished spi transaction\n");
    spi_interface_deinit();
    return 0;
}
