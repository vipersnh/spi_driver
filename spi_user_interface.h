#ifndef __SPI_USER_INTERFACE_H__
#define __SPI_USER_INTERFACE_H__

#define DEVICE_FILE_NAME "spi_device"
#define DEVICE_MAJOR_NUMBER 10
#define DEVICE_MINOR_NUMBER 10

typedef enum {
    SPI_SPEED_500Hz,
    SPI_SPEED_1KHz,
    SPI_SPEED_2KHz,
} spi_interface_speed_enum_t;

typedef enum {
    SPI_MODE_0,
    SPI_MODE_1,
    SPI_MODE_2,
    SPI_MODE_3,
} spi_interface_mode_enum_t;

typedef enum {
    SPI_CS_SEL_0,
    SPI_CS_SEL_1,
} spi_interface_cs_enum_t;

bool spi_interface_init(spi_interface_speed_enum_t speed, spi_interface_mode_enum_t mode);
bool spi_interface_deinit(void);

bool spi_write_data(
        char *write_data, 
        unsigned int write_len, 
        spi_interface_cs_enum_t cs
        );

bool spi_read_data(
        char *read_data,
        unsigned int read_len, 
        spi_interface_cs_enum_t cs
        );

bool spi_read_write_data(
        char *read_data, 
        char *write_data, 
        unsigned int transfer_len, 
        spi_interface_cs_enum_t cs
        );

#endif
