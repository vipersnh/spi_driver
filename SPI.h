#ifndef __SPI_H__ 
#define __SPI_H__


/* Generated software access code for registers in module SPI */




/* Code for register CS: At word offset 0 */

/* Read API for register CS */

#define SPI_CS_CS_READ(_base)		((((unsigned int *)(_base))[0] >> 0) & (0x3))

#define SPI_CS_CPHA_READ(_base)		((((unsigned int *)(_base))[0] >> 2) & (0x1))

#define SPI_CS_CPOL_READ(_base)		((((unsigned int *)(_base))[0] >> 3) & (0x1))

#define SPI_CS_CLEAR_READ(_base)		((((unsigned int *)(_base))[0] >> 4) & (0x3))

#define SPI_CS_CSPOL_READ(_base)		((((unsigned int *)(_base))[0] >> 6) & (0x1))

#define SPI_CS_TA_READ(_base)		((((unsigned int *)(_base))[0] >> 7) & (0x1))

#define SPI_CS_DMAEN_READ(_base)		((((unsigned int *)(_base))[0] >> 8) & (0x1))

#define SPI_CS_INTD_READ(_base)		((((unsigned int *)(_base))[0] >> 9) & (0x1))

#define SPI_CS_INTR_READ(_base)		((((unsigned int *)(_base))[0] >> 10) & (0x1))

#define SPI_CS_ADCS_READ(_base)		((((unsigned int *)(_base))[0] >> 11) & (0x1))

#define SPI_CS_REN_READ(_base)		((((unsigned int *)(_base))[0] >> 12) & (0x1))

#define SPI_CS_LEN_READ(_base)		((((unsigned int *)(_base))[0] >> 13) & (0x1))

#define SPI_CS_LMONO_READ(_base)		((((unsigned int *)(_base))[0] >> 14) & (0x1))

#define SPI_CS_TE_EN_READ(_base)		((((unsigned int *)(_base))[0] >> 15) & (0x1))

#define SPI_CS_DONE_READ(_base)		((((unsigned int *)(_base))[0] >> 16) & (0x1))

#define SPI_CS_RXD_READ(_base)		((((unsigned int *)(_base))[0] >> 17) & (0x1))

#define SPI_CS_TXD_READ(_base)		((((unsigned int *)(_base))[0] >> 18) & (0x1))

#define SPI_CS_RXR_READ(_base)		((((unsigned int *)(_base))[0] >> 19) & (0x1))

#define SPI_CS_RXF_READ(_base)		((((unsigned int *)(_base))[0] >> 20) & (0x1))

#define SPI_CS_CSPOL0_READ(_base)		((((unsigned int *)(_base))[0] >> 21) & (0x1))

#define SPI_CS_CSPOL1_READ(_base)		((((unsigned int *)(_base))[0] >> 22) & (0x1))

#define SPI_CS_CSPOL2_READ(_base)		((((unsigned int *)(_base))[0] >> 23) & (0x1))

#define SPI_CS_DMA_LEN_READ(_base)		((((unsigned int *)(_base))[0] >> 24) & (0x1))

#define SPI_CS_LEN_LONG_READ(_base)		((((unsigned int *)(_base))[0] >> 25) & (0x1))


/* Read-modify-write API for register CS */

#define SPI_CS_CS_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x3<<0)) | (((_val) & 0x3) << 0))

#define SPI_CS_CPHA_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<2)) | (((_val) & 0x1) << 2))

#define SPI_CS_CPOL_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<3)) | (((_val) & 0x1) << 3))

#define SPI_CS_CLEAR_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x3<<4)) | (((_val) & 0x3) << 4))

#define SPI_CS_CSPOL_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<6)) | (((_val) & 0x1) << 6))

#define SPI_CS_TA_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<7)) | (((_val) & 0x1) << 7))

#define SPI_CS_DMAEN_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<8)) | (((_val) & 0x1) << 8))

#define SPI_CS_INTD_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<9)) | (((_val) & 0x1) << 9))

#define SPI_CS_INTR_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<10)) | (((_val) & 0x1) << 10))

#define SPI_CS_ADCS_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<11)) | (((_val) & 0x1) << 11))

#define SPI_CS_REN_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<12)) | (((_val) & 0x1) << 12))

#define SPI_CS_LEN_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<13)) | (((_val) & 0x1) << 13))

#define SPI_CS_LMONO_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<14)) | (((_val) & 0x1) << 14))

#define SPI_CS_TE_EN_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<15)) | (((_val) & 0x1) << 15))

#define SPI_CS_DONE_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<16)) | (((_val) & 0x1) << 16))

#define SPI_CS_RXD_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<17)) | (((_val) & 0x1) << 17))

#define SPI_CS_TXD_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<18)) | (((_val) & 0x1) << 18))

#define SPI_CS_RXR_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<19)) | (((_val) & 0x1) << 19))

#define SPI_CS_RXF_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<20)) | (((_val) & 0x1) << 20))

#define SPI_CS_CSPOL0_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<21)) | (((_val) & 0x1) << 21))

#define SPI_CS_CSPOL1_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<22)) | (((_val) & 0x1) << 22))

#define SPI_CS_CSPOL2_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<23)) | (((_val) & 0x1) << 23))

#define SPI_CS_DMA_LEN_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<24)) | (((_val) & 0x1) << 24))

#define SPI_CS_LEN_LONG_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((unsigned int *)(_base))[0] & ~(0x1<<25)) | (((_val) & 0x1) << 25))


/* Write-Only API for register CS */

#define SPI_CS_CS_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x3) << 0))

#define SPI_CS_CPHA_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 2))

#define SPI_CS_CPOL_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 3))

#define SPI_CS_CLEAR_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x3) << 4))

#define SPI_CS_CSPOL_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 6))

#define SPI_CS_TA_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 7))

#define SPI_CS_DMAEN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 8))

#define SPI_CS_INTD_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 9))

#define SPI_CS_INTR_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 10))

#define SPI_CS_ADCS_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 11))

#define SPI_CS_REN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 12))

#define SPI_CS_LEN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 13))

#define SPI_CS_LMONO_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 14))

#define SPI_CS_TE_EN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 15))

#define SPI_CS_DONE_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 16))

#define SPI_CS_RXD_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 17))

#define SPI_CS_TXD_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 18))

#define SPI_CS_RXR_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 19))

#define SPI_CS_RXF_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 20))

#define SPI_CS_CSPOL0_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 21))

#define SPI_CS_CSPOL1_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 22))

#define SPI_CS_CSPOL2_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 23))

#define SPI_CS_DMA_LEN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 24))

#define SPI_CS_LEN_LONG_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[0] = (((_val) & 0x1) << 25))




/* Code for register FIFO: At word offset 1 */

/* Read API for register FIFO */

#define SPI_FIFO_DATA_READ(_base)		((((unsigned int *)(_base))[1] >> 0) & (0xffffffff))


/* Read-modify-write API for register FIFO */

#define SPI_FIFO_DATA_WRITE(_base, _val)		(((unsigned int *)(_base))[1] = (((unsigned int *)(_base))[1] & ~(0xffffffff<<0)) | (((_val) & 0xffffffff) << 0))


/* Write-Only API for register FIFO */

#define SPI_FIFO_DATA_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[1] = (((_val) & 0xffffffff) << 0))




/* Code for register CLK: At word offset 2 */

/* Read API for register CLK */

#define SPI_CLK_CDIV_READ(_base)		((((unsigned int *)(_base))[2] >> 0) & (0xffff))


/* Read-modify-write API for register CLK */

#define SPI_CLK_CDIV_WRITE(_base, _val)		(((unsigned int *)(_base))[2] = (((unsigned int *)(_base))[2] & ~(0xffff<<0)) | (((_val) & 0xffff) << 0))


/* Write-Only API for register CLK */

#define SPI_CLK_CDIV_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[2] = (((_val) & 0xffff) << 0))




/* Code for register DLEN: At word offset 3 */

/* Read API for register DLEN */

#define SPI_DLEN_LEN_READ(_base)		((((unsigned int *)(_base))[3] >> 0) & (0xffff))


/* Read-modify-write API for register DLEN */

#define SPI_DLEN_LEN_WRITE(_base, _val)		(((unsigned int *)(_base))[3] = (((unsigned int *)(_base))[3] & ~(0xffff<<0)) | (((_val) & 0xffff) << 0))


/* Write-Only API for register DLEN */

#define SPI_DLEN_LEN_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[3] = (((_val) & 0xffff) << 0))




/* Code for register LTOH: At word offset 4 */

/* Read API for register LTOH */

#define SPI_LTOH_TOH_READ(_base)		((((unsigned int *)(_base))[4] >> 0) & (0xf))


/* Read-modify-write API for register LTOH */

#define SPI_LTOH_TOH_WRITE(_base, _val)		(((unsigned int *)(_base))[4] = (((unsigned int *)(_base))[4] & ~(0xf<<0)) | (((_val) & 0xf) << 0))


/* Write-Only API for register LTOH */

#define SPI_LTOH_TOH_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[4] = (((_val) & 0xf) << 0))




/* Code for register DC: At word offset 5 */

/* Read API for register DC */

#define SPI_DC_TDREQ_READ(_base)		((((unsigned int *)(_base))[5] >> 0) & (0xff))

#define SPI_DC_TPANIC_READ(_base)		((((unsigned int *)(_base))[5] >> 8) & (0xff))

#define SPI_DC_RDREQ_READ(_base)		((((unsigned int *)(_base))[5] >> 16) & (0xff))

#define SPI_DC_RPANIC_READ(_base)		((((unsigned int *)(_base))[5] >> 24) & (0xff))


/* Read-modify-write API for register DC */

#define SPI_DC_TDREQ_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((unsigned int *)(_base))[5] & ~(0xff<<0)) | (((_val) & 0xff) << 0))

#define SPI_DC_TPANIC_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((unsigned int *)(_base))[5] & ~(0xff<<8)) | (((_val) & 0xff) << 8))

#define SPI_DC_RDREQ_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((unsigned int *)(_base))[5] & ~(0xff<<16)) | (((_val) & 0xff) << 16))

#define SPI_DC_RPANIC_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((unsigned int *)(_base))[5] & ~(0xff<<24)) | (((_val) & 0xff) << 24))


/* Write-Only API for register DC */

#define SPI_DC_TDREQ_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((_val) & 0xff) << 0))

#define SPI_DC_TPANIC_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((_val) & 0xff) << 8))

#define SPI_DC_RDREQ_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((_val) & 0xff) << 16))

#define SPI_DC_RPANIC_ONLY_WRITE(_base, _val)		(((unsigned int *)(_base))[5] = (((_val) & 0xff) << 24))

#endif